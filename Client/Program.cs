﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.AspNet.SignalR.Client;

namespace Client
{
    class Program
    {
        private static void Main(string[] args)
        {
            Console.WriteLine("Client started...");
            Entry.Start().GetAwaiter().GetResult();
            
            Console.ReadLine();
        }
    }
}
